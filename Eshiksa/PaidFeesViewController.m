//
//  PaidFeesViewController.m
//  Eshiksa
//
//  Created by Punit on 07/05/18.
//  Copyright © 2018 Akhilesh. All rights reserved.


#import "PaidFeesViewController.h"
#import "Constant.h"
#import "PaidFees.h"
#import "PaidFeesTableViewCell.h"
#import "BaseViewController.h"
#import "Base.h"
#import "WebViewController.h"
#import "StudentPaidFees.h"
#import "StudentPaidFeesTableViewCell.h"
@interface PaidFeesViewController ()
{
    BOOL shouldCellBeExpanded;
    NSInteger indexOfExpandedCell;
}


@end

@implementation PaidFeesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    shouldCellBeExpanded = NO;
    indexOfExpandedCell = -1;
     [_tableview setSeparatorColor:[UIColor clearColor]];
    
    _tableview.delegate=self;
    _tableview.dataSource=self;
    
//    UIActivityIndicatorView *indicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    indicator.frame=CGRectMake(self.view.window.center.x,self.view.window.center.y, 40.0, 40.0);
//    indicator.center=self.view.center;
//    [self.view addSubview:indicator];
//    indicator.tintColor=[UIColor redColor];
//    indicator.backgroundColor=[UIColor lightGrayColor];
//    [indicator bringSubviewToFront:self.view];
//    [indicator startAnimating];
    
    _billNumberArr=[[NSMutableArray alloc]init];
    _createdDateArr=[[NSMutableArray alloc]init];
    _feesNameArr=[[NSMutableArray alloc]init];
    _payStatusArr=[[NSMutableArray alloc]init];
    _feesAmountArr=[[NSMutableArray alloc]init];
    _fineAmountArr=[[NSMutableArray alloc]init];
    _paidAmountArr=[[NSMutableArray alloc]init];
    _paidArr=[[NSMutableArray alloc]init];
    
   [self parsingPaidFees];
  
}

-(void)parsingPaidFees{
    
    [_billNumberArr removeAllObjects];
    [_createdDateArr removeAllObjects];
    [_feesNameArr removeAllObjects];
    [_payStatusArr removeAllObjects];
    [_feesAmountArr removeAllObjects];
    [_fineAmountArr removeAllObjects];
    [_paidAmountArr removeAllObjects];
    [_paidArr removeAllObjects];
    
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in circular==%@",groupname);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    NSLog(@"circular password ==%@",password);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSLog(@"circular cyear ==%@",cyear);
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];
    NSLog(@"circular orgid ==%@",orgid);

    

    NSString *urlstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:fees_v2]];
    
    NSDictionary *parameterDict = @{
                                    @"groupname":groupname,
                                    @"username":username,
                                    @"password":password,
                                    @"dbname":dbname,
                                    @"Branch_id":branchid,
                                    @"org_id":orgid,
                                    @"cyear":cyear,
                                    @"url": urlstr,
                                    @"tag":@"paid"
                                    };
   
    
    [Constant executequery:urlstr strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            
            NSLog(@"response  paid fees data:%@",maindic);
                       
            NSArray *ciculararr=[maindic objectForKey:@"paid_fees"];
            NSLog(@"paid_fees0000:%@",ciculararr);
            
            if(ciculararr.count==0)
            {
            
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"No data available" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alertView dismissViewControllerAnimated:YES completion:nil];
                                         }];
                    [alertView addAction:ok];
                    [self presentViewController:alertView animated:YES completion:nil];
            }
            else {
                
       for (NSArray *coverUrlArray in ciculararr) {
           
            for(NSDictionary *temp in coverUrlArray)
            {
                NSString *str1=[temp objectForKey:@"bill_number"];
                NSString *str2=[[temp objectForKey:@"created_date"]description];
                NSString *str3=[[temp objectForKey:@"fees_name"]description];
                NSString *str4=[[temp objectForKey:@"pay_status"]description];
                NSString *str5=[[temp objectForKey:@"fees_amount"]description];
                NSString *str6=[[temp objectForKey:@"fine_amount"]description];
                NSString *str7=[[temp objectForKey:@"paid_amount"]description];
                NSString *str8=[[temp objectForKey:@"due_amount"]description];
                NSString *str9=[[temp objectForKey:@"student_id"]description];
                NSString *str10=[[temp objectForKey:@"first_name"]description];
                NSString *str11=[[temp objectForKey:@"last_name"]description];
                NSString *str12=[[temp objectForKey:@"admission_no"]description];
                NSString *str13=[[temp objectForKey:@"transaction_number"]description];
                NSString *str14=[[temp objectForKey:@"paid_from"]description];
                NSString *str15=[[temp objectForKey:@"total_paid_amount"]description];
                NSString *str16=[[temp objectForKey:@"receipt_download"]description];
                NSString *str17=[[temp objectForKey:@"fees_receipt"]description];
                
                NSLog(@"bill_number=%@  title=%@ publish_date=%@ publish_todate=%@",str1,str2,str3,str4);
                if (([(NSString*)str1 isEqual: [NSNull null]])) {
                    // Showing AlertView Here
                }else {
                    
                PaidFees *k1=[[PaidFees alloc]init];
                k1.billNumStr=str1;
                k1.createdDateStr=str2;
                k1.feesNameStr=str3;
                k1.payStatusStr=str4;
                k1.feesAmountStr=str5;
                k1.fineAmountStr=str6;
                k1.paidAmtStr=str7;
                    k1.dueAmtStr=str8;
                    k1.studentIdStr=str9;
                    k1.firstNameStr=str10;
                    k1.lastNameStr=str11;
                    k1.admissiomNumStr=str12;
                    k1.transactionNumStr=str13;
                    k1.paidFromStr=str14;
                    k1.totalPaidAmtStr=str15;
                    k1.reciecptDownloadStr=str16;
                    k1.feesRecieptStr=str17;

                    if ([k1.fineAmountStr containsString:@"<null>"]){
                               k1.fineAmountStr=@"0";
                    }
                    if ([k1.feesAmountStr containsString:@"<null>"]){
                        k1.feesAmountStr=@"0";
                    }
                    
                    
                [_paidArr addObject:k1];
            }
                [_tableview reloadData];
            }
           
          }
         }
        }
        [_tableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableview reloadData];
            
            //[indicator stopAnimating];
        });
    }];
 
}
-(void)createInvoicePdfParsing{
    
    NSString *studentId = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"studentId"];
    NSLog(@"studentId==%@",studentId);
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"requisition branchid ==%@",branchid);
    
    NSString *billNum = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"billNum"];
    NSLog(@"billNum ==%@",billNum);
 
    
    NSString *str=[NSString stringWithFormat:@"%@",[createInvoiceUrl stringByAppendingString:createInvoicePdf]];
    
    NSDictionary *parameterDict = @{
                                    @"student":studentId,
                                    @"bill_number":billNum,
                                    @"Branch_id":branchid
                
                                    };
    
    NSLog(@"parameter dict==%@",parameterDict);
    
    [Constant executequery:str  strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"response create invoice pgParams:%@",maindic);
        
            if([self.success isEqualToString:@"1"])
            {
                NSLog(@"Data saved succesffullyy");
               
           }
            else
            {

            }
            
        }
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    if (section==0) {
        return _paidArr.count;
    }else if(section==1){
        return _paidArr.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexOfExpandedCell==indexPath.row)
    {
    PaidFeesTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    PaidFees *ktemp=[_paidArr objectAtIndex:indexPath.row];
    
    cell.billNumber.text=ktemp.billNumStr;
    cell.createdDate.text=ktemp.createdDateStr;
    cell.feesName.text=ktemp.feesNameStr;
    cell.payStatus.text=ktemp.payStatusStr;
    cell.feesAmount.text=ktemp.feesAmountStr;
    cell.fineAmount.text=ktemp.fineAmountStr;
    cell.paidAmount.text=ktemp.paidAmtStr;
    cell.feesReciept.text=ktemp.feesRecieptStr;
    cell.studentId.text=ktemp.studentIdStr;
    
        
    NSLog(@"fees reciept url==%@",ktemp.feesRecieptStr);
    
    return cell;
    }else
    {
         StudentPaidFeesTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
        
        StudentPaidFees *ktemp=[_paidArr objectAtIndex:indexPath.row];
        
        cell.feesName.text=ktemp.feesNameStr;
        cell.paidAmount.text=ktemp.paidAmtStr;
        cell.selectBtn.tag=indexPath.row;
        
        [cell.selectBtn setTag:indexPath.row];
        
        [cell.selectBtn addTarget:self action:@selector(selectBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
    return 0;
}
- (void)viewDidLayoutSubviews{
    NSString *language = [@"" currentLanguage];
    if ([language isEqualToString:@"hi"])
    {
        [self setBackButtonLocalize];
    }
}

- (void)setBackButtonLocalize{
    self.navigationItem.title = [@"PAID_FEES" localize];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if(indexOfExpandedCell==indexPath.row)
    {
    PaidFeesTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    NSLog(@"cell==%@",cell);
    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    _billNumStr=cell.billNumber.text;
    _studentIdStr=cell.studentId.text;
    _feesRecieptStr=cell.feesReciept.text;
    
    NSString *str = [instUrl stringByAppendingString:_feesRecieptStr];
    cell.feesRecieptUrl.text=str;
    
    NSLog(@"FEES rECIEPT URL %@",str);
    
    [[NSUserDefaults standardUserDefaults] setObject:str forKey:@"feeReciept"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:_billNumStr forKey:@"billNum"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:_studentIdStr forKey:@"studentId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"indexpath==%ld",(long)indexPath.row);

    [self performSegueWithIdentifier:@"showDownload"
                              sender:[self.tableview cellForRowAtIndexPath:indexPath]];
    
     [self createInvoicePdfParsing];
    }
    
  
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    WebViewController *wvc=[segue destinationViewController];
    if ([segue.identifier isEqualToString:@"showDownload"]) {
        
        NSString *feeReciept = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"feeReciept"];
        NSLog(@"***feeReciept ==%@",feeReciept);

        wvc.myURL=feeReciept;
        
        NSLog(@"*******full downloading url str=%@",feeReciept);
        
    }
}

-(void)selectBtnClicked:(UIButton *)sender{
    
    UIButton *aButton = (UIButton *)sender;
    indexOfExpandedCell = [aButton tag];
    shouldCellBeExpanded = YES;
    
    [_tableview beginUpdates];
    [_tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexOfExpandedCell inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
    [_tableview endUpdates];
    [_tableview reloadData];
    NSLog(@"indexPath.row: %ld",aButton.tag);
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexOfExpandedCell==indexPath.row) {
        return 184.0f;
    }
    else{
        return 64.0f;
    }
    return 184.0f;
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [_tableview reloadData];
}


@end
