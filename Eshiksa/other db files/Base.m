

#import "Base.h"

@implementation Base
//maldives live url bid::6

NSString * const mainUrl = @"http://mdsapi.eshiksa.net/";
NSString * const dbname = @"esh_maldives";
NSString * const instUrl = @"https://mds.eshiksa.net";
NSString * const downloadUrl =@"https://mds.eshiksa.net/esh/reports/Finance/invoices/";
NSString * const homeworkdownloadUrl =@"https://mds.eshiksa.net/esh/reports/Finance/invoices/";
NSString * const pgUrl = @"https://mds.eshiksa.net/esh/index.php?plugin=Pay&action=index&txnid=";
NSString * const createInvoiceUrl = @"https://mds.eshiksa.net/index.php?plugin=Fees&action=";

//maldives edemo data db url Branch id::1

//NSString * const mainUrl = @"http://shop.eshiksa.com/appAPI_v2_maldives/";
//NSString * const dbname = @"erpeshik_esh_edemo_maldives";
//NSString * const instUrl = @"https://mds.eshiksa.net";
//NSString * const downloadUrl =@"http://erp.eshiksa.net/edemo_maldives/";
//NSString * const homeworkdownloadUrl =@"http://erp.eshiksa.net/edemo_maldives";
//NSString * const trackingUrl = @"http://shop.eshiksa.com/appAPI_v2_maldives/";
//NSString * const pgUrl = @"http://erp.eshiksa.net/edemo_maldives/esh/index.php?plugin=pay&action=index&txnid=";
//NSString * const createInvoiceUrl = @"https://mds.eshiksa.net/index.php?plugin=Fees&action=";

NSString * const auth = @"auth.php";
NSString * const pgParams = @"pgParams.php";
NSString * const savePGData_v2 = @"savePGData_v2.php";
NSString * const excess_amount = @"excess_amount.php";
NSString * const fees_v2 = @"fees_v2.php";
NSString * const homework = @"homework.php";
NSString * const hostel = @"hostel.php";
NSString * const leave = @"leave.php";
NSString * const gallery = @"gallery.php";
NSString * const attendance = @"attendance.php";
NSString * const profile = @"profile.php";
NSString * const newcircular = @"newcircular.php";
NSString * const subjectDetails = @"subjectDetails.php";
NSString * const studentclgtimetable = @"studentclgtimetable.php";
NSString * const change_pass = @"change_pass.php";
NSString * const library = @"library.php";
NSString * const empRequisition = @"empRequisition.php";
NSString * const teacherSalaryStructure = @"teacherSalaryStructure.php";
NSString * const teacher_homework_assign = @"teacher_homework_assign.php";
NSString * const empMyReport = @"empMyReport.php";
NSString * const teachersclattendance = @"teachersclattendance.php";
NSString * const teacherclgattendance = @"teacherclgattendance.php";
NSString * const teachersubmitattendance = @"teachersubmitattendance.php";
NSString * const teacherTimetable = @"teacherTimetable.php";
NSString * const push_notifications = @"push_notifications.php";
NSString * const force_password = @"force_password.php";
NSString * const etracki_payment = @"etracki_payment.php";
NSString * const currentroutelist = @"currentroutelist.php";
NSString * const createInvoicePdf = @"createInvoicePdf_new";

@end
