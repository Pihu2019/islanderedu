//
//  StudentPaidFees.h
//  Islanders Education
//
//  Created by Punit on 27/11/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface StudentPaidFees : NSObject
@property (nonatomic,strong) NSString *billNumStr,*createdDateStr,*feesNameStr,*payStatusStr,*feesAmountStr,*fineAmountStr,*dueAmtStr,*paidAmtStr,*invoiceBtnStr,*studentIdStr,*firstNameStr,*lastNameStr,*admissiomNumStr,*transactionNumStr,*paidFromStr,*totalPaidAmtStr,*reciecptDownloadStr,*feesRecieptStr;
@end

NS_ASSUME_NONNULL_END
