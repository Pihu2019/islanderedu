//
//  StudentParentViewController.h
//  Eshiksa
//
//  Created by Punit on 03/10/18.
//  Copyright © 2018 Akhilesh. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface StudentParentViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *profileBtn;
@property (weak, nonatomic) IBOutlet UIImageView *profileImg;
@property (weak, nonatomic) IBOutlet UIButton *galleryBtn;
@property (weak, nonatomic) IBOutlet UIImageView *galleryImg;
@property (weak, nonatomic) IBOutlet UIButton *paidBtn;
@property (weak, nonatomic) IBOutlet UIImageView *paidImg;
@property (weak, nonatomic) IBOutlet UIButton *circularBtn;
@property (weak, nonatomic) IBOutlet UIImageView *circularImg;
@property (weak, nonatomic) IBOutlet UIButton *payfeeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *payfeeImg;
@property (weak, nonatomic) IBOutlet UIButton *settingBtn;
@property (weak, nonatomic) IBOutlet UIImageView *settingImg;

@property (weak, nonatomic) IBOutlet UILabel *profiletxt;
@property (weak, nonatomic) IBOutlet UILabel *circulartext;
@property (weak, nonatomic) IBOutlet UILabel *gallerytxt;
@property (weak, nonatomic) IBOutlet UILabel *payfeestxt;
@property (weak, nonatomic) IBOutlet UILabel *paidfeestxt;
@property (weak, nonatomic) IBOutlet UILabel *settingstxt;
@property (weak, nonatomic) IBOutlet UILabel *poweredBy;

@property (nonatomic,strong) NSString *success,*email,*error,*tag,*user,*indxpath,*admisnNum,*firstName,*lastName,*studentId,*mobile,*emailId,*address,*branchId,*opyear,*picId,*orgId,*transportLicence;
@property (weak, nonatomic) IBOutlet UILabel *admissionNum;
@property (weak, nonatomic) IBOutlet UILabel *firstNamelbl;
@property (weak, nonatomic) IBOutlet UILabel *branchIdlbl;
@property (weak, nonatomic) IBOutlet UILabel *addresslbl;
@property (weak, nonatomic) IBOutlet UILabel *emaillbl;
@property (weak, nonatomic) IBOutlet UILabel *lastnamelbl;
@property (weak, nonatomic) IBOutlet UILabel *mobilelbl;
@property (weak, nonatomic) IBOutlet UILabel *opyearlbl;
@property (weak, nonatomic) IBOutlet UILabel *orgIdlbl;
@property (weak, nonatomic) IBOutlet UILabel *picIdlbl;
@property (weak, nonatomic) IBOutlet UILabel *studentIdlbl;
@property (weak, nonatomic) IBOutlet UILabel *transportLicencelbl;
@end

NS_ASSUME_NONNULL_END
