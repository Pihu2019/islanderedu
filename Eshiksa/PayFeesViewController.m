
#import "PayFeesViewController.h"
#import "Constant.h"
#import "PayFees.h"
#import "PayFeesTableViewCell.h"
#import "PayNowViewController.h"
#import "BaseViewController.h"
#import "Base.h"
#import "WebViewController.h"
#import "FeesTableViewCell.h"
#import "Fees.h"
@interface PayFeesViewController (){
    NSString *amountAdded,*amountAdded2;
    NSString *commissionAmount,*netAmt,*totalAmt,*totalCalculatedAmount;
    BOOL shouldCellBeExpanded;
    NSInteger indexOfExpandedCell;
}

@end

@implementation PayFeesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    shouldCellBeExpanded = NO;
    indexOfExpandedCell = -1;
    
    [_tableview setSeparatorColor:[UIColor clearColor]];
    _tableview.delegate=self;
    _tableview.dataSource=self;
    

    _feesNameArr=[[NSMutableArray alloc]init];
    _feesAmountArr=[[NSMutableArray alloc]init];
    _dueDateArr=[[NSMutableArray alloc]init];
    _dueAmountArr=[[NSMutableArray alloc]init];
    _paidAmountArr=[[NSMutableArray alloc]init];
    _totalConcessionAmountArr=[[NSMutableArray alloc]init];
    _headFineAmountArr=[[NSMutableArray alloc]init];
    _payArr=[[NSMutableArray alloc]init];
    
    _cardtypeStr=@"netbanking";
   [self parsingPayFees];
   
    
}
-(void)parsingPayFees{

    UIActivityIndicatorView *indicator=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.frame=CGRectMake(self.view.window.center.x,self.view.window.center.y, 40.0, 40.0);
    indicator.center=self.view.center;
    [self.view addSubview:indicator];
    
    
    indicator.tintColor=[UIColor redColor];
    indicator.backgroundColor=[UIColor lightGrayColor];
    [indicator bringSubviewToFront:self.view];
    [indicator startAnimating];
    
    [_feesNameArr removeAllObjects];
    [_feesAmountArr removeAllObjects];
    [_dueDateArr removeAllObjects];
    [_dueAmountArr removeAllObjects];
    [_paidAmountArr removeAllObjects];
    [_totalConcessionAmountArr removeAllObjects];
    [_headFineAmountArr removeAllObjects];
    [_payArr removeAllObjects];
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
    NSLog(@"group name in circular==%@",groupname);
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    NSLog(@"circular username ==%@",username);
    
    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    NSLog(@"circular password ==%@",password);
    
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
    NSLog(@"circular branchid ==%@",branchid);
    
    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];
    NSLog(@"circular cyear ==%@",cyear);
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];
    NSLog(@"circular orgid ==%@",orgid);
    
    NSString *urlstr=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:fees_v2]];
    
    
    NSDictionary *parameterDict = @{
                                    @"groupname":groupname,
                                    @"username":username,
                                    @"password":password,
                                    @"dbname":dbname,
                                    @"Branch_id":branchid,
                                    @"org_id":orgid,
                                    @"cyear":cyear,
                                    @"url": urlstr,
                                    @"tag":@"due"
                                    };
      NSLog(@"*****parameterDict:%@",parameterDict);
    
    [Constant executequery:urlstr strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
        NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            
            NSLog(@"response  due_fees  data:%@",maindic);
            
            NSArray *ciculararr=[maindic objectForKey:@"due_fees"];
            NSLog(@"due_fees:%@",ciculararr);
     
            if(ciculararr.count==0)
            {
               
                    UIAlertController *alertView = [UIAlertController alertControllerWithTitle:@"Sorry!" message:@"No data available" preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* ok = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             [alertView dismissViewControllerAnimated:YES completion:nil];
                                             
                                         }];
                    
                    
                    [alertView addAction:ok];
                    
                    [self presentViewController:alertView animated:YES completion:nil];
                
            }
        else {
            for (NSArray *coverUrlArray in ciculararr) {
    
            for(NSDictionary *temp in coverUrlArray)
            {
                NSString *str1=[[temp objectForKey:@"fees_name"]description];
                NSString *str2=[[temp objectForKey:@"fees_amount"]description];
                NSString *str3=[[temp objectForKey:@"due_date"]description];
                NSString *str4=[[temp objectForKey:@"due_amount"]description];
                NSString *str5=[[temp objectForKey:@"paid_amount"]description];
                NSString *str6=[[temp objectForKey:@"total_concession_amount"]description];
                NSString *str7=[[temp objectForKey:@"head_fine_amount"]description];
                NSString *str8=[[temp objectForKey:@"batch_id"]description];
                NSString *str9=[[temp objectForKey:@"course_id"]description];
                NSString *str10=[[temp objectForKey:@"department_id"]description];
                NSString *str11=[[temp objectForKey:@"session_id"]description];
                NSString *str12=[[temp objectForKey:@"student_id"]description];
                NSString *str13=[[temp objectForKey:@"fees_id"]description];
                NSString *str14=[[temp objectForKey:@"online_pay_discount"]description];

                NSLog(@"fees_name=%@  fees_amount=%@ due_date=%@ due_amount=%@ paid_amount=%@ total concession_amount=%@ head_fine_amount=%@ batch_id=%@ course_id=%@ department_id=%@ session_id=%@ student_id=%@ fees_id=%@ online_pay_discount=%@",str1,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11,str12,str13,str14);

                double result = [str4 doubleValue] - [str6 doubleValue] + [str7 doubleValue];
                totalCalculatedAmount =[NSString stringWithFormat:@"%.02f", result];
                
                NSLog(@"****totalCalculatedAmount  %@",totalCalculatedAmount);
                
                    PayFees *k1=[[PayFees alloc]init];
                    k1.fees_nameStr=str1;
                    k1.fees_amountStr=str2;
                    k1.due_dateStr=str3;
                    k1.due_amountStr=str4;
                    k1.paid_amountStr=str5;
                    k1.total_concession_amountStr=str6;
                    k1.head_fine_amountStr=str7;
                    k1.batchIdStr=str8;
                    k1.courseIdStr=str9;
                    k1.departmentIdStr=str10;
                    k1.sessionIdStr=str11;
                    k1.studentIdStr=str12;
                    k1.feesIdStr=str13;
                    k1.onlinePayDiscStr=str14;
                    k1.totalCalculatedAmountStr=totalCalculatedAmount;
                
                    [_payArr addObject:k1];
            
               }
                    [_tableview reloadData];
             }
            }
        }
        
        [_tableview performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableview reloadData];
            
            [indicator stopAnimating];
        });
    }];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return _payArr.count;
    }else if(section==1){
        return _payArr.count;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexOfExpandedCell==indexPath.row)
      {
        
    PayFeesTableViewCell *cell = [_tableview dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    PayFees *ktemp=[_payArr objectAtIndex:indexPath.row];
    
    cell.fees_name.text=ktemp.fees_nameStr;
    cell.fees_amount.text=ktemp.fees_amountStr;
    cell.due_date.text=ktemp.due_dateStr;
    cell.due_amount.text=ktemp.due_amountStr;
    cell.paid_amount.text=ktemp.paid_amountStr;
    cell.total_concession_amount.text=ktemp.total_concession_amountStr;
    cell.head_fine_amount.text=ktemp.head_fine_amountStr;
    cell.courseid.text=ktemp.courseIdStr;
    cell.batchid.text=ktemp.batchIdStr;
    cell.departmentId.text=ktemp.departmentIdStr;
    cell.sessionId.text=ktemp.sessionIdStr;
    cell.studentId.text=ktemp.studentIdStr;
    cell.feesId.text=ktemp.feesIdStr;
    cell.onlinepayDiscountAmt.text=ktemp.onlinePayDiscStr;
    cell.totalCalculatedAmt.text=ktemp.totalCalculatedAmountStr;
    return cell;
    }
        else
        {
            
           FeesTableViewCell *cell=[_tableview dequeueReusableCellWithIdentifier:@"cell2" forIndexPath:indexPath];
            
           Fees *ktemp=[_payArr objectAtIndex:indexPath.row];

           cell.feesName.text=ktemp.fees_nameStr;
           cell.dueAmount.text=ktemp.due_amountStr;
           cell.selectBtn.tag=indexPath.row;
            
            [cell.selectBtn setTag:indexPath.row];
            
            [cell.selectBtn addTarget:self action:@selector(selectBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }

    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexOfExpandedCell==indexPath.row)
    {
    PayFeesTableViewCell *cell=[_tableview cellForRowAtIndexPath:indexPath];
    
    _sessionIdStr=cell.sessionId.text;
    _studentIdStr=cell.studentId.text;
    _departmentIdStr=cell.departmentId.text;
    _batchIdStr=cell.batchid.text;
    _courseIdStr=cell.courseid.text;
    _feesAmtStr=cell.fees_amount.text;
    _dueAmtStr=cell.due_amount.text;
    _totalConcessionAmtStr=cell.total_concession_amount.text;
    _headFineAmountStr=cell.head_fine_amount.text;
    _feesIdStr=cell.feesId.text;
    
    _indxp=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    
    [[NSUserDefaults standardUserDefaults] setObject:_feesAmtStr forKey:@"feeAmount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:_headFineAmountStr forKey:@"headFineAmount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    double result = [_dueAmtStr doubleValue] - [_totalConcessionAmtStr doubleValue] + [_headFineAmountStr doubleValue];
    totalAmt =[NSString stringWithFormat:@"%.02f", result];


    NSString *pgnetbanking = [[NSUserDefaults standardUserDefaults]
                              stringForKey:@"pgnetbanking"];
    

    double result2=[totalAmt doubleValue]+[pgnetbanking doubleValue];
    netAmt = [NSString stringWithFormat:@"%.02f", result2];

        float roundedValue = [netAmt floatValue];
        
        NSString *floatString = [NSString stringWithFormat:@"%.1f",roundedValue];
        
        NSLog(@"floatString==%@",floatString);
        
    
    [[NSUserDefaults standardUserDefaults] setObject:floatString forKey:@"netAmount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setObject:cell.departmentId.text forKey:@"departmentId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     [self pgParamsDataParsing];
   
    }
    
}

-(void)pgParamsDataParsing{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];
   
    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];

    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];

    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];

    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
    
    NSString *str=[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:pgParams]];
    
    NSDictionary *parameterDict = @{
                                    @"groupname":groupname,
                                    @"username":username,
                                    @"instUrl":instUrl,
                                    @"dbname":dbname,
                                    @"Branch_id":branchid,
                                    @"org_id": orgid,
                                    @"cyear": cyear,
                                    @"url":str,
                                    @"tag":@"fees",
                                    @"password": password,
                                    @"course_id":_courseIdStr
                                    };

    [Constant executequery:str  strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
       // NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
          
            _tag=[maindic objectForKey:@"tag"];
            _success=[[maindic objectForKey:@"success"]stringValue];
            _error=[[maindic objectForKey:@"error"]stringValue];
            _merchantId=[maindic objectForKey:@"merchantId"];
            _pgURL=[maindic objectForKey:@"pgURL"];
            _pgAction=[maindic objectForKey:@"pgAction"];
            _instId=[maindic objectForKey:@"instId"];
            _instName=[maindic objectForKey:@"instname"];
            _pgName=[maindic objectForKey:@"pgName"];
            _encryptKey=[maindic objectForKey:@"encryptKey"];
            _pgCCComission=[maindic objectForKey:@"pgCCComission"];
            _pgDCComission=[maindic objectForKey:@"pgDCComission"];
            _pgNBComissionStr=[maindic objectForKey:@"pgNBComission"];
            _pgUrlStr=[maindic objectForKey:@"appPgURL"];
            

            [[NSUserDefaults standardUserDefaults] setObject:_encryptKey forKey:@"encryptkey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:_pgName forKey:@"pgname"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:_merchantId forKey:@"merchantid"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:_pgNBComissionStr forKey:@"pgnetbanking"];
            [[NSUserDefaults standardUserDefaults] synchronize];
         
            
            if([self.success isEqualToString:@"1"])
            {

                  [self savePgParamsDataParsing];
            }
            else
            {
                 NSLog(@"Something went wrong..");

            }
            
        }
    }];
}
-(void)savePgParamsDataParsing{
    
    NSString *groupname = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"groupName"];

    NSString *branchid = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"branchid"];
 
    
    NSString *orgid = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"orgid"];

    NSString *cyear = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"cyear"];

    NSString *password = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"password"];

    NSString *username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"username"];
   
    NSString *pgname = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"pgname"];

    NSString *encryptkey = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"encryptkey"];

    NSString *merchantid = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"merchantid"];

    NSString *departmentId = [[NSUserDefaults standardUserDefaults]
                              stringForKey:@"departmentId"];
   
    NSString *feeAmt = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"feeAmount"];

    NSString *headFineAmount = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"headFineAmount"];
  
    NSString *pgnetbanking = [[NSUserDefaults standardUserDefaults]
                              stringForKey:@"pgnetbanking"];
    
    _udf1 = [NSString stringWithFormat: @"%@%s%@%s%@%s%@%s%@%s%@%s%@%s%@%s%@%s%d", _feesIdStr,"_",_studentIdStr,"_",_courseIdStr,"_",_batchIdStr,"_",feeAmt,"_",pgnetbanking,"_",headFineAmount,"_",departmentId,"_",username,"_",0];
    
    NSLog(@"UDF1 DATA==%@ ",_udf1);
    NSString *netAmount = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"netAmount"];
   
    NSString *str =[NSString stringWithFormat:@"%@",[mainUrl stringByAppendingString:savePGData_v2]];
    
    
    NSDictionary *parameterDict = @{

      @"amount":netAmount,
      @"Branch_id":branchid,
      @"dbname":dbname,
      @"groupname":groupname,
      @"instUrl":instUrl,
      @"key":merchantid,
      @"password":password,
      @"payMethod":_cardtypeStr,
      @"pgName":pgname,
      @"salt":encryptkey,
      @"surl":mainUrl,
      @"tag":@"savePGData",
      @"udf1":_udf1,
      @"udf2":dbname,
      @"udf3":branchid,
      @"url":str,
      @"user_name":username
                                    };
    
    NSLog(@"parameter dict==%@",parameterDict);
    
    [Constant executequery:str  strpremeter:parameterDict withblock:^(NSData * dbdata, NSError *error) {
       // NSLog(@"data:%@",dbdata);
        if (dbdata!=nil) {
            NSDictionary *maindic=[NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
            NSLog(@"response data in save pg...:%@",maindic);
            
            _tag=[maindic objectForKey:@"tag"];
            _success=[[maindic objectForKey:@"success"]stringValue];
            _error=[maindic objectForKey:@"error"];
            _tempIdStr=[maindic objectForKey:@"tempId"];
            _txnIdStr=[maindic objectForKey:@"txnid"];
            _concessionStatusStr=[maindic objectForKey:@"concession_status"];
            
        

          NSString  *transactionID=_txnIdStr;
            NSLog(@"IN API txnid---%@",transactionID);
            [[NSUserDefaults standardUserDefaults] setObject:transactionID forKey:@"transactionIdStr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"***transactionId***** = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"transactionIdStr"]);
            
            [self performSegueWithIdentifier:@"showPayInfo" sender:nil];
            
               if([self.success isEqualToString:@"1"])
             {
             NSLog(@"success.....");
                 
             }
             else
             {
             NSLog(@"failure.....");
             }
        }
    }];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    WebViewController *wvc=[segue destinationViewController];
    if ([segue.identifier isEqualToString:@"showPayInfo"]) {
        
      NSString *txnid = [[NSUserDefaults standardUserDefaults]
                           stringForKey:@"transactionIdStr"];
      NSLog(@"txnid==%@",txnid);
        NSString *branchid = [[NSUserDefaults standardUserDefaults]
                              stringForKey:@"branchid"];

       NSString *netAmount = [[NSUserDefaults standardUserDefaults]stringForKey:@"netAmount"];
        
         NSLog(@"netAmount==%@",netAmount);
       
        
        NSString *myst=[NSString stringWithFormat:@"plugin=Pay&action=index&txnid=%@&branch_id=%@&amount=%@",txnid,branchid,netAmount];
        NSLog(@"**myst String %@ ",myst);
        
        NSString *str = [@"https://mds.eshiksa.net/esh/index.php?" stringByAppendingString:myst];
        
    
        
        wvc.myURL=str;
        
        NSLog(@"*******full str=%@",wvc.myURL);


    }
}
- (void)viewDidLayoutSubviews{
    NSString *language = [@"" currentLanguage];
    if ([language isEqualToString:@"hi"])
    {
        [self setBackButtonLocalize];
    }
}

- (void)setBackButtonLocalize{
    self.navigationItem.title = [@"PAY_FEES" localize];
}
-(void)selectBtnClicked:(UIButton *)sender{
    
    UIButton *aButton = (UIButton *)sender;
    indexOfExpandedCell = [aButton tag];
    shouldCellBeExpanded = YES;
    
    [_tableview beginUpdates];
    [_tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem: indexOfExpandedCell inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
    [_tableview endUpdates];
    [_tableview reloadData];
    NSLog(@"indexPath.row: %ld",aButton.tag);

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexOfExpandedCell==indexPath.row) {
        return 216.0f;
    }
    else{
        return 64.0f;
    }
return 216.0f;

}
- (void)viewWillAppear:(BOOL)animated
{
    [_tableview reloadData];
}


@end
/*
 "Branch_id" = 1;
 amount = "600.00";
 dbname = "esh_maldives";
 groupname = Student;
 instUrl = "https://mds.eshiksa.net";
 key = 9809710775;
 password = bA3kc2;
 payMethod = netbanking;
 pgName = MALDIVEPG;
 salt = 407387;
 surl = "http://mdsapi.eshiksa.net/";
 tag = savePGData;
 udf1 = "2110 _ 5016 _ 51 _ 55 _ 550 _ 50 _ 0 _ 3 _ maya15016165 _ 0";
 udf2 = "esh_maldives";
 udf3 = 1;
 url = "http://mdsapi.eshiksa.net/savePGData_v2.php";
 "user_name" = maya15016165;
 
https://mds.eshiksa.net/esh/index.php?plugin=Pay&action=index&txnid=esha_ea74757b83cd4e38a517&branch_id=1&amount=600.00 //something went wrong
 
 "Branch_id" = 1;
 amount = "600.00";
 dbname = "esh_maldives";
 groupname = Student;
 instUrl = "https://mds.eshiksa.net";
 key = 9809710775;
 password = bA3kc2;
 payMethod = netbanking;
 pgName = MALDIVEPG;
 salt = 407387;
 surl = "http://mdsapi.eshiksa.net/";
 tag = savePGData;
 udf1 = "2110 _ 5016 _ 51 _ 55 _ 550 _ 50 _ 0 _ 3 _ maya15016165 _ 0";
 udf2 = "esh_maldives";
 udf3 = 1;
 url = "http://mdsapi.eshiksa.net/savePGData_v2.php";
 "user_name" = maya15016165;
 
https://mds.eshiksa.net/esh/index.php?plugin=Pay&action=index&txnid=esha_d638e2a458cfd3446da6&branch_id=1&amount=600.00 //working
 
 
 */
